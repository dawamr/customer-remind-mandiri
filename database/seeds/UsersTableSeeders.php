<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Admin';
        $user->email = 'admin@podcash.top';
        $user->password = bcrypt('rahasia');
        $user->save();
    }
}
