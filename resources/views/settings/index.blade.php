@extends('layouts.master')

@section('style')
<link href="{{ asset('css/summernote-bs4.min.css') }}" rel="stylesheet">
<script src="{{ asset('js/summernote-bs4.min.js') }}"></script>
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<style>
    .select2 .select2-container {
        width: 100% !important;
    }
</style>
@endsection

@section('header')

<div class="section-header">
    <h1>Customers List</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Customers</h4>
                     {{-- <div class="card-header-action">
                        <a href="{{ url('admin/customers/create') }}" class="btn btn-info">Add <i class="fas fa-plus"></i></a>
                    </div> --}}
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-4">
                            <ul class="nav nav-pills flex-column" id="myTab4" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab1" data-toggle="tab" href="#message" role="tab" aria-controls="message" aria-selected="true">Message</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab2" data-toggle="tab" href="#variable" role="tab" aria-controls="variable" aria-selected="false">Variable</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab3" data-toggle="tab" href="#device" role="tab" aria-controls="device" aria-selected="false">Device</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 col-md-8">
                            <div class="tab-content no-padding" id="myTab2Content">
                                <div class="tab-pane fade show active" id="message" role="tabpanel" aria-labelledby="tab1">
                                    <div class="buttons mb-1">
                                        <button id="btEdit" class="btn btn-warning" onclick="edit()" type="button">Edit</button>
                                        <button id="btSave" class="btn btn-info" onclick="save()" type="button">Save</button>
                                    </div>
                                    <div id="note-message">{!! @$lastChat->message_html !!}</div>
                                    <div>
                                        <b>Tips :</b>
                                        <ul>
                                            <li>Shift + Enter: &nbsp; &nbsp; &nbsp; New Line</li>
                                            <li>* : <strong>*Bold*</strong></li>
                                            <li>_ : <i>_Italic_</i>,</li>
                                            <li>~ : <strike>~Strike~</strike></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="variable" role="tabpanel" aria-labelledby="tab2">
                                    <form action="{{ url('admin/configs') }}" method="post">
                                        @csrf
                                        <b>Create New Variable</b><br>
                                        <label for="">Variable</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">@</span>
                                            </div>
                                            <input class="form-control" name="variable" type="text" required>
                                        </div>
                                        <label for="">Domain</label>
                                        <div class="input-group mb-3 row">
                                            <div class="col-md-4">
                                                <label for="">Table</label>
                                                <div class="input-group">
                                                    <select name="table_name" class="form-control" id="tableName" required>
                                                        <option value="">-- select option --</option>
                                                        <option value="customers">Customers</option>
                                                        <option value="assets">Assets</option>
                                                        <option value="asset_details">Asset Details</option>
                                                        <option value="dates">Dates</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <label for="">Column</label>
                                                <div class="input-group">
                                                    <select name="column_name" class="form-control" id="columnName" required>
                                                        <option value="">-- select option --</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <a href="{{ url('admin/configs') }}" class="btn btn-secondary">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                    <hr>
                                    <div class="table-responsive">
                                        <b>List Variable</b>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Variable</th>
                                                    <th>Domain</th>
                                                    @if(\Auth::user()->email == 'admin@podcash.top')
                                                    <th>Action</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($configs as $config)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{ $config->variable }}</td>
                                                    <td>{{ $config->table_name }} -> {{ $config->column_name }}</td>
                                                    @if(\Auth::user()->email == 'admin@podcash.top')    
                                                    <td>
                                                        <form action="{{ url('admin/settings/variable/'.$config->id)}}" method="post" onsubmit="return confirm('Are you sure delete ?');">
                                                            @csrf 
                                                            @method('DELETE')
                                                            <button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-trash"></i></button>
                                                        </form>
                                                    </td>
                                                    @endif  
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade show" id="device" role="tabpanel" aria-labelledby="tab3">
                                    <table class="table table-striped table-responsive">
                                        <tr>
                                            <th>Device Number</th>
                                            <th>Key</th>
                                        </tr>
                                        <tr>
                                            <td>{{ env('APP_PHONE_DEVICE') }}</td>
                                            <td>{{ env('APP_WA_KEY') }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    let _token = $('input[name="_token"]').val();
    let arrData = "{{ json_encode($arrData)}}";
    let arr = JSON.parse(arrData.replace(/&quot;/g,'"'));
    var option = {
            focus: true,
            placeholder: 'Typing...',
            toolbar: [
                ['style', []],
            ],
            hint: {
                mentions: arr,
                match: /\B@(\w*)$/,
                search: function (keyword, callback) {
                callback($.grep(this.mentions, function (item) {
                    return item.indexOf(keyword) == 0;
                }));
                },
                content: function (item) {
                return '@' + item;
                }    
            }
    }
    $('#note-message').summernote(option);

    $('#type_config').change(()=>{
        if ($('#type_config').val() == 'addm') {
            $('#tenor_type_config').replaceWith('<select name="tenor_type" class="form-control" id="tenor_type_config"><option value="11">11</option><option value="23">23</option><option value="35">35</option><option value="47">47</option></select>')
        }
        if ($('#type_config').val() == 'addb') {
            $('#tenor_type_config').replaceWith('<select name="tenor_type" class="form-control" id="tenor_type_config"><option value="12">12</option><option value="24">24</option><option value="36">36</option><option value="48">48</option></select>')
        }
    });
    
    var edit = function() {
        $('#note-message').summernote(option);
    };

    var save = function() {
        var markup = $('#note-message').summernote('code');
        $('#note-message').summernote('destroy');
        $.ajax({
            url : "{{ URL::current() }}",
            method : "POST",
            data : {
                    _token : _token,
                    message : $('#note-message').html()
                },
            success : function(result){
                    alert('success update the message !');
                    console.log(result);
                },
        });
    };

    window.onload = (event) => {
        $('.select2-container').width('100%')
    };
    $(document).ready(function() {
        $('#tableName').change(()=>{
            $.getJSON( "{{ url('api/config') }}/" + $('#tableName').val() +'/listColumn', function (data, status) {
                if(status === 'success'){
                    let option = '<select name="column_name" class="form-control" id="columnName">'+data+'</select>'
                $('#columnName').replaceWith(option);
                }else{
                    $('#columnName').replaceWith('<select name="column_name" class="form-control" id="columnName"></select>')
                }
            })
            .done(function() { console.log('getJSON request succeeded!'); })
            .fail(function(jqXHR, textStatus, errorThrown) { $('#columnName').replaceWith('<select name="column_name" class="form-control" id="columnName"></select>'); alert('getJSON request failed! ' + textStatus); })

        });

        var data = [];
            function formatState (state) {
                console.log(state);
                if (!state.id) {
                return state.text;
                }
                var $state = $(
                '<span>' +state.text+'</span>'
                );
                return $state;
            };
    
            $(function(){
                $("#search_asset").select2({
                    minimumInputLength: 2,
                    templateResult: formatState, //this is for append country flag.
                    placeholder: 'typing @ for use variable...',
                    cache: true,
                    ajax: {
                        url: "{{ url('api/assets') }}",
                        dataType: 'json',
                        type: "GET",
                        data: function (params) {
                            // console.log(term)
                            return {
                                q: $.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.text,
                                        id: item.id,
                                    }
                                })
                            };
                        }
    
                    }
                });
            });

    });
  </script>
@endsection