@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Customer Edit</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Customers</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/customers/'. $customer->id) }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/customers/'.$customer->id) }}" method="post">
                        @csrf 
                        @method('PUT')
                        @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                        @elseif (session('wrong'))
                            <div class="alert alert-danger">
                                {{ session('wrong') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputName">Full Name</label>
                                    <input type="text" class="form-control" id="inputName" name="name" value="{{ $customer->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" id="inputEmail" name="email" value="{{ $customer->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone">Phone</label>
                                    <input type="text" class="form-control" id="inputPhone" name="phone" value="{{ $customer->phone }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                        <label for="inputPlaceBirth">Place Of Birth</label>
                                        <input type="text" class="form-control" id="inputPlaceBirth" name="place_of_birth" value="{{ $customer->place_of_birth }}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputDateBirth">Date Of Birth</label>
                                            <input type="date" class="form-control" id="inputDateBirth" name="date_of_birth" value="{{ $customer->date_of_birth }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputGender">Gender</label>
                                    <select name="gender" class="form-control" id="inputGender">
                                        <option @if($customer->gender == 'm') selected @endif value="m">Man</option>
                                        <option @if($customer->gender == 'w') selected @endif value="w">Women</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress">Address</label>
                                    <textarea class="form-control" name="address" id="inputAddress">{{ $customer->address }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone">Join</label>
                                    <input type="date" class="form-control" id="inputJoin" name="join_date" value="{{ $customer->join_date }}">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('admin/customers/'.$customer->id) }}" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ asset('js/simple.moneyFormat.js') }}"></script>
<script>
    $(document).ready(()=>{
        $('.price').simpleMoneyFormat();
        
    });
</script>
@endsection