@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}" /> 
@endsection

@section('header')

<div class="section-header">
    <h1>Customers List</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Customers</h4>
                    <div class="card-header-action">
                        @if(\Auth::user()->email == 'admin@podcash.top')
                        <a href="{{ url('admin/customers/create') }}" class="btn btn-info">Add <i class="fas fa-plus"></i></a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Full Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Total Asset</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($customers as $customer)
                                        @php 
                                            $total_asset = \App\Asset::where('customer_id', $customer->id)->count()
                                        @endphp
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $customer->name }}</td>
                                            <td>{{$customer->phone}}</td>
                                            <td>{{$customer->email}}</td>
                                            <td>{{ $total_asset }}</td>
                                            <td>
                                                <form action="{{ url('admin/customers/'. $customer->id) }}" method="post" onsubmit="return confirm('Are you sure delete ?');">
                                                    @csrf
                                                    @method('DELETE')
                                                        <a href="{{ url('admin/customers/'. $customer->id) }}" data-toggle="tooltip" data-placement="bottom" title="Detail Profile"  class="btn btn-outline-info"><i class="fas fa-user"></i></a>
                                                        {{-- <a id="modal_info" data-toggle="tooltip" data-placement="bottom" title="Send Message" onclick="sendMessage('{{$customer->id}}')" class="btn btn-outline-success"><i class="fab fa-telegram-plane"></i></a> --}}
                                                        @if(\Auth::user()->email == 'admin@podcash.top')
                                                        <button data-toggle="tooltip" data-placement="bottom" title="Delete Customer"  type="submit" class="btn btn-outline-danger"><i class="fas fa-trash"></i>   </button>
                                                        @endif
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{asset('stisla/assets/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script>
        function sendMessage(id_customer){
            alert('Message is being sent')
            $.getJSON("{{ url('api/customers') }}" + "/" + id_customer + "?auth={{Auth::id()}}", function (data, status) {
                console.log(status)
                alert('Message has been sent !');
            })
            .done(function() { console.log('success!'); })
            .fail(function(jqxhr, textStatus, error) {
                var err = textStatus + ", " + error;
                alert( "Request Failed: " + err );
            })
            .always(function() {
                console.log( "complete" );
            });
        }
        $(document).ready(() => {
            $("table").dataTable({
                columnDefs: [{
                    sortable: true,
                    targets: [0, 1]
                }]
            });

        });
    </script>
@endsection