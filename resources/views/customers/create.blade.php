@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Customers Create</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Customers</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/assets') }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/customers') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputName">Full Name</label>
                                    <input type="text" class="form-control" id="inputName" name="name" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone">Phone</label>
                                    <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="">
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                        <label for="inputPlaceBirth">Place Of Birth</label>
                                        <input type="text" class="form-control" id="inputPlaceBirth" name="place_of_birth" placeholder="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputDateBirth">Date Of Birth</label>
                                            <input type="date" class="form-control" id="inputDateBirth" name="date_of_birth" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputGender">Gender</label>
                                    <select name="gender" class="form-control" id="inputGender">
                                        <option value="m">Man</option>
                                        <option value="w">Women</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress">Address</label>
                                    <textarea class="form-control" name="address" id="inputAddress"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone">Join</label>
                                    <input type="date" class="form-control" id="inputJoin" name="join_date" placeholder="">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('admin/customers') }}" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection