@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Customer Profile</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Customer {{ $customer->name }}</h4>
                    <div class="card-header-action">
                        @if(\Auth::user()->email == 'admin@podcash.top')
                        <a href="{{ url('admin/customers/'.$customer->id.'/edit') }}" class="btn btn-warning">Edit</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-primary">
                        {{ session('success') }}
                    </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="inputName">Customer Name</label>
                                <br><strong class="pl-3">{{ $customer->name }}</strong>
                            </div>
                            <div class="form-group">
                                <label for="inputYear">Phone</label>
                                <br><strong class="pl-3">{{ $customer->phone }}</strong>
                            </div>
                            <div class="form-group">
                                <label for="inputPrice">Email</label>
                                <br><strong class="pl-3">{{ $customer->email }}</strong>
                            </div>
                            <div class="form-group">
                                <label for="inputPrice">Address</label>
                                <br><strong class="pl-3">{{ $customer->address }}</strong>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="inputName">Place, Date Of Brith</label>
                                <br><strong class="pl-3">{{ $customer->place_of_birth }}, {{ $customer->date_of_birth }}</strong>
                            </div>
                            <div class="form-group">
                                <label for="inputYear">Gender</label>
                                <br><strong class="pl-3">@if($customer->gender == 'm') Man @elseif($customer->gender == 'woman') Woman @endif</strong>
                            </div>
                            <div class="form-group">
                                <label for="inputPrice">Join</label>
                                <br><strong class="pl-3">{{ $customer->join_date }}</strong>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <p>Asset Informations</p>
                            <div class="angsuran">
                                <div class="col-12">
                                    @if(\Auth::user()->email == 'admin@podcash.top')
                                    <a href="{{ url('admin/customers/'.$customer->id.'/assets/create') }}" class="btn btn-primary btn-sm text-white">Add New</a>
                                    @endif
                                    <div class="table-responsive mt-3">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Asset Name</th>
                                                    <th>Cotract Number</th>
                                                    <th>Price</th>
                                                    <th>Year</th>
                                                    <th>WA Blast</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($assets as $data)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $data->name }}</td>
                                                    <td>{{ @$data->no_contract }}</td>
                                                    <td>{{ number_format($data->price) }}</td>
                                                    <td>{{ $data->year }}</td>
                                                    <td>
                                                        <div class="button-group">
                                                            @if($data->on_set == 'Y')
                                                            <a href="{{ url('admin/customers/'.$customer->id . '?wa_blast=inactive&asset_id='.$data->id) }}" data-toggle="tooltip" data-placement="bottom" title="Active"  class="btn btn-sm btn-success">ON</a>
                                                            @else
                                                            <a href="{{ url('admin/customers/'.$customer->id . '?wa_blast=active&asset_id='.$data->id) }}" data-toggle="tooltip" data-placement="bottom" title="Not Active"  class="btn btn-sm btn-danger">OFF</a>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <form action="{{ url('admin/customers/'.$data->customer_id.'/assets/'.$data->id) }}" method="post" onsubmit="return confirm('Are you sure delete ?');">
                                                            @csrf 
                                                            @method('DELETE')
                                                            <div class="button-group">
                                                                <a href="{{ url('admin/customers/'.$customer->id.'/assets/'. $data->id . '/edit') }}" data-toggle="tooltip" data-placement="bottom" title="Detail Asset"  class="btn btn-outline-info">Detail Assets</a>
                                                                @if(\Auth::user()->email == 'admin@podcash.top')
                                                                <button class="btn btn-danger" type="submit">Delete</button>
                                                                @endif
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>

</script>
@endsection