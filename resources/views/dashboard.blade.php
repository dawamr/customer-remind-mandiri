@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Dashboard</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-primary">
                    <i class="fas fa-mobile-alt"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Assets</h4>
                    </div>
                    <div class="card-body">
                        {{ \App\Asset::count()   }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-danger">
                    <i class="fas fa-sticky-note"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Sended</h4>
                    </div>
                    <div class="card-body">
                        {{ \App\ChatHistory::count() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-purple">
                    <i class="fas fa-user"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Employee</h4>
                    </div>
                    <div class="card-body">
                        {{ \App\User::count() -1 }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-12">
            <div class="card card-statistic-1">
                <div class="card-icon bg-warning">
                    <i class="fas fa-users"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Customer</h4>
                    </div>
                    <div class="card-body">
                        {{ \App\Customer::count() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection