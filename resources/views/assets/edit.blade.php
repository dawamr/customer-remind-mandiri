@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Assets Edit</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Assets</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/customers/'.$customer->id) }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-primary">
                        {{ session('success') }}
                    </div>
                    @elseif (session('wrong'))
                    <div class="alert alert-danger">
                        {{ session('wrong') }}
                    </div>
                    @endif
                        <div class="row">
                            <div class="col-md-8">
                                <form action="{{ url('admin/customers/'.$customer->id.'/assets/'.$asset->id) }}" method="post">
                                        @csrf 
                                        @method('PUT')
                                    <div class="form-group">
                                        <label for="inputName">Contract Number</label>
                                        <input type="text" class="form-control" id="inputName" name="name" readonly placeholder="" value="{{ @$asset->no_contract }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputName">Asset Name</label>
                                        <input type="text" class="form-control" id="inputName" name="name" placeholder="" value="{{ $asset->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputYear">Year</label>
                                        <input type="text" class="form-control" id="inputYear" name="year" placeholder="" value="{{ $asset->year }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPrice">Price</label>
                                        <input type="text" class="form-control price" id="inputPrice" name="price" placeholder="" value="{{ $asset->price }}">
                                    </div>
                                    <div class="form-group">
                                        <a href="{{ url('admin/customers/'.$customer->id) }}" class="btn btn-secondary">Cancel</a>
                                        @if(\Auth::user()->email == 'admin@podcash.top')
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        @endif
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputName">Customer</label>
                                    <input type="text" class="form-control" id="inputName" name="customer_name"  value="{{ $customer->name }}" readonly>
                                </div>
                            </div>
                        <div class="col-md-12">
                            <hr>
                            <p>Another Informations</p>
                        
                            <div class="angsuran">
                                <div class="col-12">
                                    @if(\Auth::user()->email == 'admin@podcash.top')
                                    <a href="{{ url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/config/create/') }}" class="btn btn-info mb-3">Add New</a>
                                    @endif
                                    <br>
                                    @php
                                        $addb = array("year" => 1,"tenor" => 12,);
                                        $addm = array("year" => 1,"tenor" => 11,);
                                    @endphp
                                    <div class="card">
                                        <form action="{{  url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/config') }}" method="post">
                                            @csrf 
                                            <div class="card-header">
                                                <h4>ADDB</h4>
                                                @if(\Auth::user()->email == 'admin@podcash.top')
                                                <div class="card-header-action">
                                                    <button type="submit" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Save All"><i class="fas fa-save"></i></button>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive mt-3">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Year</th>
                                                                <th>Tenor</th>
                                                                <th>Pencairan</th>
                                                                <th>Angsuran</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @for ($i = 1; $i <= 4; $i++)
                                                            <tr>
                                                                <td>{{ $i }})</td>
                                                                <td>{{ $i }}</td>
                                                                <td>
                                                                    {{ intval($addb['tenor']) * $i  }}
                                                                    <input type="hidden" name="tenor[]" value="{{ intval($addb['tenor']) * $i  }}">
                                                                    <input type="hidden" name="type[]" value="addm">
                                                                    <input type="hidden" name="id[]" value="{{ @$asset_addb[$i-1]->id }}">
                                                                </td>
                                                                <td><input @if(\Auth::user()->email !== 'admin@podcash.top') readonly @endif type="text" name="pencairan[]" class="form-control price" value="@if(@$asset_addb[$i-1]->year == $i){{ $asset_addb[$i-1]->pencairan }}@endif"></td>
                                                                <td><input @if(\Auth::user()->email !== 'admin@podcash.top') readonly @endif type="text" name="angsuran[]" class="form-control price" value="@if(@$asset_addb[$i-1]->year == $i){{ $asset_addb[$i-1]->angsuran }}@endif"></td>
                                                            </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="card">
                                        <form action="{{  url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/config') }}" method="post">
                                            @csrf
                                            <div class="card-header">
                                                <h4>ADDM</h4>
                                                <div class="card-header-action">
                                                    @if(\Auth::user()->email == 'admin@podcash.top')
                                                    <button type="submit" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Save All"><i class="fas fa-save"></i></button>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="table-responsive mt-3">
                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Year</th>
                                                                <th>Tenor</th>
                                                                <th>Pencairan</th>
                                                                <th>Angsuran</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @for ($i = 1; $i <= 4; $i++)
                                                            <tr>
                                                                <td>{{ $i }})</td>
                                                                <td>{{ $i }}</td>
                                                                <td>
                                                                    {{ (intval($addb['tenor']) * $i) -1  }}
                                                                    <input type="hidden" name="tenor[]" value="{{ (intval($addb['tenor']) * $i) -1  }}">
                                                                    <input type="hidden" name="type[]" value="addm">
                                                                    <input type="hidden" name="id[]" value="{{ $asset_addm[$i-1]->id }}">
                                                                </td>
                                                                <td><input @if(\Auth::user()->email !== 'admin@podcash.top') readonly @endif type="text" name="pencairan[]" class="form-control price" value="@if(@$asset_addm[$i-1]->year == $i){{ $asset_addm[$i-1]->pencairan }}@endif"></td>
                                                                <td><input @if(\Auth::user()->email !== 'admin@podcash.top') readonly @endif type="text" name="angsuran[]" class="form-control price" value="@if(@$asset_addm[$i-1]->year == $i){{ $asset_addm[$i-1]->angsuran }}@endif"></td>
                                                            </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="{{ asset('js/simple.moneyFormat.js') }}"></script>
<script>
    $(document).ready(()=>{
        $('.price').simpleMoneyFormat();
        
    });
</script>
@endsection