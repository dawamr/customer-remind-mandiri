@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Config Create</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Assets {{$asset->name}}</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/edit') }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/config') }}" method="post">
                        @csrf 
                        @if (session('wrong'))
                            <div class="alert alert-danger">
                                {{ session('wrong') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputPrice">Type</label>
                                    <select class="form-control" name="type" id="">
                                        <option value="">-- select option --</option>  
                                        <option value="addm">ADDM</option>
                                        <option value="addb">ADDB</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPrice">Tenor</label>
                                    <input type="text" class="form-control" id="inputName" name="tenor">
                                </div>
                                <div class="form-group">
                                    <label for="">Estimasi Pencairan</label>
                                    <input type="text" class="form-control price" id="pencairan" name="pencairan">
                                </div>
                                <div class="form-group">
                                    <label for="">Estimasi Angsuran</label>
                                    <input type="text" class="form-control price" id="angsuran" name="angsuran">
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="{{ url('admin/customers/'.$asset->customer_id.'/assets/'.$asset->id.'/edit') }}" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/simple.moneyFormat.js') }}"></script>
<script>
    $(document).ready(()=>{
        $('.price').simpleMoneyFormat();
    });
</script>
@endsection