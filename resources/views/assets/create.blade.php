@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Assets Create</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Assets</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/customers/'. $customer_id ) }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/customers/'.$customer_id.'/assets') }}" method="post">
                        @csrf 
                        @if (session('wrong'))
                            <div class="alert alert-danger">
                                {{ session('wrong') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputName">Contract Number</label>
                                    <input type="text" class="form-control" id="inputName" name="no_contract"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Asset Name</label>
                                    <input type="text" class="form-control" id="inputName" name="name"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputYear">Year</label>
                                    <input type="integer" class="form-control" id="inputYear" name="year" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputPrice">Price</label>
                                    <input type="text" class="form-control price" id="inputPrice" name="price" placeholder="">
                                </div>
                                <input type="hidden" name="customer_id" value="{{ $customer_id }}">
                                <div class="form-group">
                                    <a href="{{ url('admin/customers/'.$customer_id) }}" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputName">Customer</label>
                                    <input type="text" class="form-control" id="inputName" name="customer_name"  value="{{ $customer->name }}" readonly>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/simple.moneyFormat.js') }}"></script>
<script>
    $(document).ready(()=>{
        $('.price').simpleMoneyFormat();
    });
</script>
@endsection