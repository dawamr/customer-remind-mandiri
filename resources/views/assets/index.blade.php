@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}" /> 
@endsection

@section('header')

<div class="section-header">
    <h1>Assets List</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Assets</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/assets/create') }}" class="btn btn-info">Add <i class="fas fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Year</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($assets as $asset)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $asset->name }}</td>
                                            <td>{{ $asset->year }}</td>
                                            <td>Rp. {{ number_format($asset->price) }}</td>
                                            <td>
                                                <form action="{{ url('admin/assets/'. $asset->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE ')
                                                    <div class="btn-group">
                                                        <a class="btn btn-outline-primary">Show</a>
                                                        <a href="{{ url('admin/assets/'.$asset->id.'/edit') }}" class="btn btn-outline-warning">Edit</a>
                                                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{asset('stisla/assets/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script>
        $(document).ready(() => {
            $("table").dataTable({
                columnDefs: [{
                    sortable: true,
                    targets: [0, 1]
                }]
            });

            $('.alert').delay( 1000 ).fadeOut( 400 );
        });
    </script>
@endsection