
@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}" /> 
@endsection

@section('header')

<div class="section-header">
    <h1>Employee List</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Employee</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/employee/create') }}" class="btn btn-info">Add <i class="fas fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NIP</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ str_replace('@podcash.top', '', $user->email) }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>
                                                {{-- [reset password][edit] --}}
                                                <form action="{{ url('admin/employee/'. $user->id) }}" method="post" onsubmit="return confirm('Are you sure delete ?');">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a data-toggle="tooltip" data-placement="bottom" title="Edit Employee" href="{{ url('admin/employee/'.$user->id.'/edit') }}" class="btn btn-outline-warning btn-sm"><i class="fas fa-user-edit"></i></a>
                                                    <a data-toggle="tooltip" data-placement="bottom" title="Reset Password" href="{{ url('admin/employee/'.$user->id.'/reset') }}" class="btn btn-outline-primary btn-sm" onclick="return confirm('Are you sure reset password employee ?');"><i class="fas fa-recycle"></i></a>
                                                    <button data-toggle="tooltip" data-placement="bottom" title="Delete Employee"  type="submit" class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i>   </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{asset('stisla/assets/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script>
        $(document).ready(() => {
            $("table").dataTable({
                columnDefs: [{
                    sortable: true,
                    targets: [0, 1]
                }]
            });

            $('.alert').delay( 1000 ).fadeOut( 400 );
        });
    </script>
@endsection