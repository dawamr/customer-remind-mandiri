@extends('layouts.master')

@section('style')
    
@endsection

@section('header')

<div class="section-header">
    <h1>Employee Edit</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Employee</h4>
                    <div class="card-header-action">
                        <a href="{{ url('admin/employee') }}" class="btn btn-info">Back</a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/employee/'. $employee->id ) }}" method="post">
                        {{ csrf_field() }}
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="inputNIP">NIP</label>
                                    <input type="text" class="form-control" id="inputNIP" name="nip" value="{{ str_replace('@podcash.top', '', $employee->email) }}" readonly placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone">Phone</label>
                                    <input type="text" class="form-control" id="inputPhone" name="phone" value="{{ $employee->phone }}" required placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="inputName">Full Name</label>
                                    <input type="text" class="form-control" id="inputName" name="name" value="{{ $employee->name }}" required placeholder="">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('admin/employee') }}" class="btn btn-secondary">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection