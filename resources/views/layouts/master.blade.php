<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport" />
        <title>App Customer Remind - {{ Date('Y') }}</title>

        <!-- General CSS Files -->
        <link rel="stylesheet" href="{{ asset('stisla/assets/modules/bootstrap/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('stisla/assets/modules/fontawesome/css/all.min.css') }}" />
        <script src="{{ asset('stisla/assets/modules/jquery.min.js') }}"></script>

        <!-- CSS Libraries -->
        @yield('style')

        <!-- Template CSS -->
        <link rel="stylesheet" href="{{ asset('stisla/assets/css/style.css') }}" />
        <link rel="stylesheet" href="{{ asset('stisla/assets/css/components.css') }}" />
        {{-- <link rel="stylesheet" href="{{ asset('css/stisla/custom.min.css') }}" /> --}}
        <style>
            .bg-purple{
                background-color: #621055
            }
            .btn-purple{
                background-color: #621055
            }
            .btn-outline-purple{
                border-color: #621055
                color: #621055
            }
            .preloader {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background-color: rgba(59, 59, 59, 0.24);
            }
            .preloader .loading {
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%,-50%);
                font: 14px arial;
            }
        </style>
        <!-- Start GA -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag("js", new Date());

            gtag("config", "UA-94034622-3");
        </script>
        <!-- /END GA -->
    </head>

    <body>
        <div id="app">
            <div class="preloader">
                <div class="loading">
                    <img src="{{ asset('uploads/ajax-loader.gif') }}" width="45">
                    {{-- <p>Waiting...</p> --}}
                </div>
            </div>
            <div class="main-wrapper">
                <div class="navbar-bg" style="background: #30b5e8 !important;"></div>
                <nav class="navbar navbar-expand-lg main-navbar">
                    <form class="form-inline mr-auto">
                        <ul class="navbar-nav mr-3">
                            <li>
                                <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
                            </li>
                            <li>
                                <a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a>
                            </li>
                        </ul>
                    </form>
                    <ul class="navbar-nav navbar-right">
                        <li class="dropdown dropdown-list-toggle">
                            <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
                            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                                <div class="dropdown-header">
                                    Notifications
                                    <div class="float-right">
                                        <a href="#">Mark All As Read</a>
                                    </div>
                                </div>
                                <div class="dropdown-list-content dropdown-list-icons">
                                    <p class="text-muted p-2 text-center">No notifications found!</p>
                                    {{--
                                    <a href="#" class="dropdown-item dropdown-item-unread">
                                        <div class="dropdown-item-icon bg-primary text-white">
                                            <i class="fas fa-code"></i>
                                        </div>
                                        <div class="dropdown-item-desc">
                                            Template update is available now!
                                            <div class="time text-primary">2 Min Ago</div>
                                        </div>
                                    </a>
                                    --}}
                                </div>
                                {{--
                                <div class="dropdown-footer text-center">
                                    <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                                </div>
                                --}}
                            </div>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                                <img alt="image" src="{{ asset('stisla/assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1" />
                                <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <div class="dropdown-title">Logged in 5 min ago</div>
                                <a href="features-profile.html" class="dropdown-item has-icon"> <i class="far fa-user"></i> Profile Settings </a>
                                <div class="dropdown-divider"></div>
                                    <a href="{{ url('logout') }}" class="dropdown-item has-icon text-danger"> <i class="fas fa-sign-out-alt"></i> Logout </a>
                            </div>
                        </li>
                    </ul>
                </nav>
                <div class="main-sidebar sidebar-style-2">
                    <aside id="sidebar-wrapper">
                        <div class="sidebar-brand">
                            <a href="{{ url('admin') }}">PODCASH</a>
                        </div>
                        <div class="sidebar-brand sidebar-brand-sm">
                            <a href="{{ url('admin') }}">.TOP</a>
                        </div>
                        <ul class="sidebar-menu">
                            <li class="menu-header">{{ strtoupper('Your Navigation Menu Here !') }}</li>
                            <li>
                                <a href="{{ url('admin/dashboard') }}" class="nav-link"><i class="fa fa-columns"></i> <span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="{{ url('admin/customers') }}" class="nav-link"><i class="fa fa-users"></i> <span>Customer</span></a>
                            </li>
                            <li>
                                <a href="{{ url('admin/messages') }}" class="nav-link"><i class="fas fa-comment " aria-hidden="true"></i> <span>Messages</span></a>
                            </li>
                            <li>
                                <a href="{{ url('admin/settings') }}" class="nav-link"><i class="fa fa-envelope"></i> <span>Setting</span></a>
                            </li>
                            @if(\Auth::user()->email == 'admin@podcash.top')
                            <li class="menu-header">Admin</li>
                            <li>
                                <a href="{{ url('admin/employee') }}" class="nav-link"><i class="fa fa-users"></i> <span>Employee</span></a>
                            </li>
                            @endif
                        </ul>
                    </aside>
                </div>

                <!-- Main Content -->
                <div class="main-content">
                    <section class="section">

                        @yield('header')

                        @yield('body')

                    </section>
                </div>

                <footer class="main-footer">
                    <div class="footer-left">
                        Copyright &copy; 2020 Mandiri
                        <div class="bullet"></div>
                        Created By <a href="https://www.dawam.us/">Dawam Raja</a>
                    </div>
                    <div class="footer-right"></div>
                </footer>
            </div>
        </div>

        <!-- General JS Scripts -->
        <script src="{{ asset('stisla/assets/modules/popper.js') }}"></script>
        <script src="{{ asset('stisla/assets/modules/tooltip.js') }}"></script>
        <script src="{{ asset('stisla/assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('stisla/assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('stisla/assets/modules/moment.min.js') }}"></script>
        <script src="{{ asset('stisla/assets/js/stisla.js') }}"></script>

        <!-- JS Libraies -->
        @yield('script')

        <!-- Page Specific JS File -->
        <script>
            setTimeout(function(){ 
                $('.alert').fadeOut(1500);
             }, 2000);
             $(document).ready(()=>{
                $(".preloader").fadeOut(2000);
             });
        </script>

        <!-- Template JS File -->
        <script src="{{ asset('stisla/assets/js/scripts.js') }}"></script>
        <script src="{{ asset('stisla/assets/js/custom.js') }}"></script>
    </body>
</html>
