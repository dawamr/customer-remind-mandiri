@extends('layouts.master')

@section('style')
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/datatables.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('stisla/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}" /> 
@endsection

@section('header')

<div class="section-header">
    <h1>Asset List</h1>
</div>

@endsection

@section('body')
<div class="section-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Messages</h4>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @elseif (session('wrong'))
                        <div class="alert alert-danger">
                            {{ session('wrong') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <form action="{{ url('admin/messages') }}" id="form-check" method="post">
                                    @csrf
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <td style="white-space: nowrap;">
                                                    Total Selected :
                                                </td>
                                                <td colspan="2">
                                                    <div class="row">
                                                        <div class="col-auto">
                                                            <span id="counter">0</span>
                                                            <button disabled id="sendAll" type="submit" class="btn btn-primary btn-sm ml-3">Send All</button>
                                                        </div>
                                                        <div class="col-auto" style="padding-top : 7px" >
                                                            <input type="checkbox" name="" id="select_all"> Select All
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <select name="filter_type" id="filter_type" class="form-control">
                                                        <option value="addb">ADDB</option>
                                                        <option value="addm">ADDM</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th width="15%">Action</th>
                                                <th width="25%">Contract Number</th>
                                                <th width="30%">Customer Name</th>
                                                <th width="30%">Asset Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($assets as $asset)
                                            <tr>
                                                <td>
                                                    <label class="colorinput">
                                                        <input type="checkbox" value="{{ $asset->id }}" name="asset_id[]" class="colorinput-input">
                                                        <span class="colorinput-color bg-info"></span>
                                                    </label>
                                                </td>
                                                <td style="vertical-align: top; padding-top: 1.5em"> {{$asset->no_contract}} </td>
                                                <td style="vertical-align: top; padding-top: 1.5em">{{ $asset->customer_name }}</td>
                                                <td style="vertical-align: top; padding-top: 1.5em">{{ $asset->name }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{asset('stisla/assets/modules/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('stisla/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
    <script>
        function updateCounter() {
            var len = $("input[name='asset_id[]']:checked").length;
            console.log(len)
            if(len>0){
                $("#counter").text('['+len+']');
                $('#sendAll').replaceWith('<button id="sendAll" onclick="sendMessage()" type="submit" class="btn btn-primary btn-sm ml-3">Send All</button>')
            }else{
                $("#counter").text('0');
                $('#sendAll').replaceWith('<button disabled id="sendAll"  type="submit" class="btn btn-primary btn-sm ml-3">Send All</button>')
            }
        }
        $('#select_all').change(()=>{
            var checkthis = $('#select_all');
            var checkboxes = $("input[name='asset_id[]']");
            if (checkthis.is(':checked')) {
                checkboxes.attr('checked', true);
            } else {
                checkboxes.attr('checked', false);
            }
            updateCounter();
        });
        function loading(){
            $(".preloader").show();
        }
        $("input:checkbox").on("change", function() {
            updateCounter();
        });
        function sendMessage(){
            $(".preloader").show();
        }
        $('#filter_type').change(()=>{
            location.href = "{{ URL::current() }}" + "?type=" + $('#filter_type').val()
        });
        @if(!empty(request()->get('type')))
            $('#filter_type').val( "{{request()->get('type')}}" );
        @endif
        $(document).ready(() => {
            $("table").dataTable({
                columnDefs: [{
                    sortable: true,
                    targets: [0, 1]
                }]
            });
        });
    </script>
@endsection