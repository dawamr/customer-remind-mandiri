<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('logout', 'HomeController@logout');

Route::group(['prefix'=>'admin', 'middleware' => ['auth']], function(){
    Route::get('/', function(){
        return redirect('admin/dashboard');
    });
    Route::get('dashboard', function(){
        return view('dashboard');
    });
    Route::post('customers/{customer_id}/assets/{asset_id}/config', 'AssetDetailController@update');
    Route::resource('customers/{customer_id}/assets', 'AssetController');
    Route::resource('customers/{customer_id}/assets/{asset_id}/config', 'AssetConfigController');
    Route::resource('customers', 'CustomerController');
    Route::resource('settings', 'ChatController');
    Route::delete('settings/variable/{variable_id}', 'ChatController@desroy_variable');
    Route::post('settings/assets', 'ChatController@settingAssets');
    Route::resource('configs', 'ChatConfigController');
    Route::resource('messages', 'MessageController');
    Route::resource('employee', 'UserController');
    Route::get('employee/{id}/reset', 'UserController@password_reset');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
