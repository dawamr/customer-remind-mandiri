<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use SoftDeletes;
    //
    public function asset_details()
    {
        return $this->hasMany('App\AssetDetail');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

}
