<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetDetail extends Model
{
    //
    use SoftDeletes;

    public function asset()
    {
        return $this->belongsTo('App\Asset');
    }
}
