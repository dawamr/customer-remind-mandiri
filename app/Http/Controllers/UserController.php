<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Service\FunctionController;

class UserController extends Controller
{
    public function __construct(FunctionController $func)
    {
        $this->function = $func;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('email','<>','admin@podcash.top')->get();
        return view('employee.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phone = $this->function->formatPhoneNumber($request->phone);
        $otp = $request->password;
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->nip . '@podcash.top';
        $user->password = \bcrypt($otp);
        $user->phone = $phone;
        $user->save();
        $this->function->sendWA($phone, '*Admin telah menambahkan kamu!* %0A%0A Kamu dapat login ke aplikasi dengan NIP : '. str_replace("@podcash.top", "", $request->nip) . ' Password : '.$otp );

        return  redirect('admin/employee/')->with('success', 'Success create new employee!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['employee'] = User::find($id);
        return view('employee.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone = $this->function->formatPhoneNumber($request->phone);
        $otp = 'podcash'. rand(100,999);
        $user = User::find($id);
        if($request->nip == '+x'.$request->nip){
            $user->email = $request->nip . '@podcash.top';
        }
        $user->name = $request->name;
        $user->password = \bcrypt($otp);
        $user->phone = $phone;
        $user->save();
        return  redirect('admin/employee/')->with('success', 'Success update employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return  back()->with('success', 'Success delete employee');
    }

    public function password_reset($id)
    {
        $user = User::find($id);
        $otp = 'podcash'. rand(100,999);
        $user->password = bcrypt($otp);
        $user->save();
        $this->function->sendWA($user->phone, '*Admin telah mereset password kamu!*%0A%0A Kamu dapat login ke aplikasi dengan NIP : '. str_replace("@podcash.top", "", $user->email) . ' Password : '.$otp );

        return back()->with('success', 'Success reset employee password ! password : '. $otp);
    }
}
