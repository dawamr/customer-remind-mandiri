<?php

namespace App\Http\Controllers;

use App\Asset;
use App\AssetDetail;
use App\Customer;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($customer_id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer_id)
    {
        $data['customer_id'] = $customer_id;
        $data['customer'] = Customer::find($customer_id);
        return view('assets.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $customer_id)
    {
        try {
            $data = new Asset;
            $data->no_contract = $request->no_contract;
            $data->name = $request->name;
            $data->price = str_replace(',','',$request->price);
            $data->year = \intval($request->year);
            $data->customer_id = $request->customer_id;
            $data->save();

            for ($i=1; $i <= 4 ; $i++) {
                $detail = new AssetDetail();
                $detail->asset_id = $data->id;
                $detail->type = 'addm';
                $detail->year = $i;
                $detail->tenor = 11 * ($i);
                $detail->angsuran = 0;
                $detail->pencairan = 0;
                $detail->save();
            }

            for ($i=1; $i <= 4 ; $i++) {
                $detail = new AssetDetail();
                $detail->asset_id = $data->id;
                $detail->type = 'addb';
                $detail->year = $i;
                $detail->tenor = 12 * ($i);
                $detail->angsuran = 0;
                $detail->pencairan = 0;
                $detail->save();
            }

            return  redirect('admin/customers/'.$request->customer_id.'/assets/'.$data->id.'/edit')->with('success', 'Success create new assets');
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $customer_id, Asset $asset)
    {
        $data['asset_addb'] = AssetDetail::where([['type','addb'], ['asset_id', $asset->id]])->orderBy('year', 'asc')->get();
        $data['asset_addm'] = AssetDetail::where([['type','addm'], ['asset_id', $asset->id]])->get();
        $data['customer'] = Customer::find($customer_id);
        $data['asset'] = $asset;

        return view('assets.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update($customer_id, Request $request, $id)
    {
        try {
            $data = Asset::find($id);
            $data->name = $request->name;
            if($request->no_contract == '+x'.$request->no_contract){
                $data->no_contract = $request->no_contract;
            }
            $data->price = str_replace(',','',$request->price);
            $data->year = intval($request->year);
            
            if($data->save()){
                return  redirect('admin/customers/'.$customer_id.'/assets/'.$data->id.'/edit')->with('success', 'Success update assets');
            }
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id, Asset $asset)
    {
        try {
            if($asset->delete()){
                return  redirect('admin/assets')->with('success', 'Success delete asset ' . $asset->name . '!');
            }
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    public function listAsset(){
        $data = Asset::select('name as text', 'id as id')->get();

        return response()->json($data);
    }
}
