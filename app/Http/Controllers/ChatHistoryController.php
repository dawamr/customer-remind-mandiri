<?php

namespace App\Http\Controllers;

use App\ChatHistory;
use Illuminate\Http\Request;

class ChatHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatHistory  $chatHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ChatHistory $chatHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatHistory  $chatHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatHistory $chatHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatHistory  $chatHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatHistory $chatHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatHistory  $chatHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatHistory $chatHistory)
    {
        //
    }
}
