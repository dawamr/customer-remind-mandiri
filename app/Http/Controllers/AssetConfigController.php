<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetDetail;

class AssetConfigController extends Controller
{
    public function create($customer_id, $id){
        $data['asset'] = Asset::find($id);
        return view('assets.config.create')->with($data);
    }

    public function store(Request $request, $customer_id, $asset_id){
        for ($i=0; $i < count($request->id); $i++) { 
            $asset_detail = AssetDetail::find($request->id[$i]);
            $asset_detail->pencairan = str_replace(',','',$request->pencairan[$i]);
            $asset_detail->angsuran = str_replace(',','',$request->angsuran[$i]);
            $asset_detail->save();
        }

        return redirect('admin/customers/'.$customer_id.'/assets/'.$asset_id.'/edit');
    }

    public function destroy($customer_id, $asset_id, $id){
        $data = AssetDetail::find($id);
        $data->delete();

        return \redirect()->back()->with('success', 'Success deleted!');;
    }
}
