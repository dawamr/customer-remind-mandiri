<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Asset;
use App\Chat;
use App\ChatConfig;
use App\AssetDetail;
use App\ChatHistory;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Service\FunctionController;

class CustomerController extends Controller
{

    public function __construct(FunctionController $func)
    {
        $this->function = $func;
    }

    /**
     * Display a listing of the resource.
     *cmd
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('name','asc')->get();
        $data['customers'] = $customers;
        return view('customers.index')->with($data);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $phone = $this->function->formatPhoneNumber($request->phone);
            $data = new Customer;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->phone = $phone;
            $data->address = $request->address;
            $data->place_of_birth = $request->place_of_birth;
            $data->date_of_birth = $request->date_of_birth;
            $data->gender = $request->gender;
            $data->join_date = $request->join_date;
            
            if($data->save()){
                return  redirect('admin/customers')->with('success', 'Success create new customer');
            }
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Customer $customer)
    {
        $data['customer'] = $customer;
        $data['assets'] = Asset::where('customer_id', $customer->id)->get();
        if(!empty($request->query('wa_blast')) && !empty($request->query('asset_id'))){
            if($request->query('wa_blast') == 'active' && $request->query('asset_id') != null){
                $asset = Asset::find($request->query('asset_id'));
                $asset->on_set = 'Y';
                $asset->save();
                return redirect('admin/customers/'.$customer->id)->with('success', 'Success upade setting asset');
            }
            if($request->query('wa_blast') == 'inactive' && $request->query('asset_id') != null){
                $asset = Asset::find($request->query('asset_id'));
                $asset->on_set = 'N';
                $asset->save();
                return redirect('admin/customers/'.$customer->id)->with('success', 'Success upade setting asset');
            }
        }
        return view('customers.detail')->with($data)->with('success', 'Success upade setting asset');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $data['customer'] = $customer;
        return view('customers.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        try {
            $phone = $this->function->formatPhoneNumber($request->phone);
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone = $phone;
            $customer->address = $request->address;
            $customer->place_of_birth = $request->place_of_birth;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->gender = $request->gender;
            $customer->join_date = $request->join_date;
            
            if($customer->save()){
                return  redirect('admin/customers/'. $customer->id)->with('success', 'Success edit customer');
            }
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        try {
            if($customer->delete()){
                return  redirect('admin/customers')->with('success', 'Success delete customer ' . $customer->name . '!');
            }
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    public  function listCustomer(Request $request, $id){
        try {
            $customer = Customer::find($id);
            $chat = Chat::where('user_id', $request->query('auth'))->first();
            $assets = Asset::where('customer_id', $customer->id)->where('on_set', 'Y')->get();
            
            for ($i=0; $i < count($assets); $i++) { 
                $message_format = $this->formatMessage($customer, $assets, $i);
                $message = \str_replace($message_format['find'], $message_format['replace'], $chat->message_whatsapp);
                $sendWa = $this->function->sendWA($customer->phone, $message);
                if($sendWa !== null){
                    $chat_history = new ChatHistory;
                    $chat_history->customer_id = $customer->id;
                    $chat_history->user_id = $request->query('auth');
                    $chat_history->receiver = $customer->phone;
                    $chat_history->device = env('APP_PHONE_DEVICE');
                    $chat_history->message = $chat->message_html;
                    $chat_history->type = 'chat';
                    $chat_history->file_name = null;
                    $chat_history->file_url = null;
                    $chat_history->save();
                }
            }
            return response()->json($sendWa, 200);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage()  . '. Line :' .$th->getLine(), 400);
        }
        
    }

    public function formatMessage($customer, $assets, $index){
        
        $variables = ChatConfig::get();
        $find = [];  $replace = [];

        for ($i=0; $i < count($variables); $i++) { 
            array_push($find, '@'. $variables[$i]->variable);
            if($variables[$i]->table_name == 'customers'){
                array_push($replace, Customer::where('id',$customer->id)->pluck($variables[$i]->column_name)->first());
            }
            if($variables[$i]->table_name == 'assets'){
                if($variables[$i]->column_name == 'price'){
                    array_push($replace, number_format(Asset::where('id', $assets[$index]->id)->pluck($variables[$i]->column_name)->first()));
                }else{
                    array_push($replace, Asset::where('id', $assets[$index]->id)->pluck($variables[$i]->column_name)->first());
                }
            }
            if($variables[$i]->table_name == 'asset_details'){
                for ($j=1; $j <= 6 ; $j++) { 
                    $exists = strpos($variables[$i]->variable, '['.$j.']');
                    if ($exists !== false) {
                        if($variables[$i]->column_name == 'pencairan' || $variables[$i]->column_name == 'angsuran'){
                            array_push($replace, number_format(AssetDetail::where('asset_id', $assets[$index]->id)->where('year', $j)->pluck($variables[$i]->column_name)->first()));
                        }else{
                            array_push($replace, AssetDetail::where('asset_id', $assets[$index]->id)->where('year', $j)->pluck($variables[$i]->column_name)->first());
                        }
                    }
                }
            }
        }

        return array("find" => $find, "replace" => $replace);
    }
    
}
