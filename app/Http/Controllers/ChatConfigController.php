<?php

namespace App\Http\Controllers;

use App\ChatConfig;
use Illuminate\Http\Request;

class ChatConfigController extends Controller
{

    public function listColumn($table){
        if($table == 'customers'){
            $columns = \Schema::getColumnListing('customers');
            $resp = '';
            for ($i=0; $i < count($columns); $i++) { 
                $resp .= '<option value="'.$columns[$i].'">'.strtolower($columns[$i]).'</option>';
            }
            return response()->json($resp, 200);
        }
        if($table == 'assets'){
            $columns = \Schema::getColumnListing('assets');
            
            $resp = '';
            for ($i=0; $i < count($columns); $i++) { 
               $resp .= '<option value="'.$columns[$i].'">'.strtolower($columns[$i]).'</option>';
            }
            return response()->json($resp, 200);
        }
        if($table == 'asset_details'){
            $columns = \Schema::getColumnListing('asset_details');
            
            $resp = '';
            for ($i=0; $i < count($columns); $i++) { 
               $resp .= '<option value="'.$columns[$i].'">'.strtolower($columns[$i]).'</option>';
            }
            return response()->json($resp, 200);
        }
        if($table == 'dates'){
            $columns = array(
                0 => 'year',
                1 => 'month',
                2 => 'day'
            );
            $resp = '';
            for ($i=0; $i < count($columns); $i++) { 
                $resp .= '<option value="'.$columns[$i].'">'.strtolower($columns[$i]).'</option>';
            }
            return response()->json($resp, 200);
        }
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->table_name == 'asset_details' && $request->column_name == 'pencairan' || $request->column_name == 'angsuran'){
                for ($i=1; $i <= 6; $i++) { 
                    $data = new ChatConfig;
                    $data->variable = $request->variable . '['.$i.']';
                    $data->table_name = $request->table_name;
                    $data->column_name = $request->column_name;
                    $data->save();
                }
            }else{
                $data = new ChatConfig;
                $data->variable = $request->variable;
                $data->table_name = $request->table_name;
                $data->column_name = $request->column_name;
                $data->save();
            }
            return  redirect('admin/settings')->with('success', 'Success create new variable');
        } catch (Expection $err) {
            return  back()->with('wrong', 'Failed! '. $err->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatConfig  $chatConfig
     * @return \Illuminate\Http\Response
     */
    public function show(ChatConfig $chatConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatConfig  $chatConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatConfig $chatConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatConfig  $chatConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatConfig $chatConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatConfig  $chatConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatConfig $chatConfig)
    {
        //
    }

    
}
