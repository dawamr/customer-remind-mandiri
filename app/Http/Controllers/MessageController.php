<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Service\FunctionController;
use App\Chat;
use App\ChatConfig;
use App\ChatHistory;
use App\Customer;
use App\Asset;
use App\AssetDetail;
use Auth;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function __construct(FunctionController $func)
    {
        $this->function = $func;
    }

    public function index(){
        $data['assets'] = Asset::where([['on_set', 'Y'], ['customers.deleted_at', null]])->join('customers','customers.id','=','assets.customer_id')->select('assets.id as id', 'assets.name as name', 'assets.no_contract as no_contract', 'customers.name as customer_name')->orderBy('customer_name', 'Asc')->get();
        return view('messages.index')->with($data);
    }

    public function store(Request $request){
        try {
            $chat = Chat::where('user_id', Auth::id())->first();
            $variables = ChatConfig::get();
            for ($i=0; $i < count($request->asset_id); $i++) { 
                $asset = Asset::find($request->asset_id[$i]);
                $message_format = $this->formatMessage($variables, $asset->customer_id, $asset->id, $request->filter_type); 
                $message = \str_replace($message_format['find'], $message_format['replace'], $chat->message_whatsapp);

                $sendWa = $this->function->sendWA($asset->customer->phone, $message);
                $chat_history = new ChatHistory;
                $chat_history->customer_id = $asset->customer_id;
                $chat_history->user_id = Auth::id();
                $chat_history->receiver = $asset->customer->phone;
                $chat_history->device = env('APP_PHONE_DEVICE');
                $chat_history->message = $chat->message_html;
                $chat_history->type = 'chat';
                $chat_history->file_name = null;
                $chat_history->file_url = null;
                $chat_history->save();
            }
            return  back()->with('success', $sendWa);
        } catch (\Throwable $th) {
            return  back()->with('wrong', 'Failed! '. $th->getMessage());
        }
    }

    public function formatMessage($variables, $customer_id, $asset_id, $type = 'addb'){
        $find = [];  $replace = [];

        for ($i=0; $i < count($variables); $i++) { 
            array_push($find, '@'. $variables[$i]->variable);
            if($variables[$i]->table_name == 'customers'){
                array_push($replace, Customer::where('id',$customer_id)->pluck($variables[$i]->column_name)->first());
            }
            if($variables[$i]->table_name == 'assets'){
                if($variables[$i]->column_name == 'price'){
                    array_push($replace, number_format(Asset::where('id', $asset_id)->pluck($variables[$i]->column_name)->first()));
                }else{
                    array_push($replace, Asset::where('id', $asset_id)->pluck($variables[$i]->column_name)->first());
                }
            }
            if($variables[$i]->table_name == 'asset_details'){
                for ($j=1; $j <= 6 ; $j++) { 
                    $exists = strpos($variables[$i]->variable, '['.$j.']');
                    if ($exists !== false) {
                        if($variables[$i]->column_name == 'pencairan' || $variables[$i]->column_name == 'angsuran'){
                            array_push($replace, number_format(AssetDetail::where('asset_id', $asset_id)->where('year', $j)->where('type', $type)->pluck($variables[$i]->column_name)->first()));
                        }else{
                            array_push($replace, AssetDetail::where('asset_id', $asset_id)->where('year', $j)->where('type', $type)->pluck($variables[$i]->column_name)->first());
                        }
                    }
                }
            }
        }
        return array("find" => $find, "replace" => $replace);
    }
}
