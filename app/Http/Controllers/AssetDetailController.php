<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Service\FunctionController;

use Illuminate\Http\Request;

class AssetDetailController extends Controller
{

    public function __construct(FunctionController $func)
    {
        $this->function = $func;
    }

    public function update(Request $request, $customer_id, $asset_id){
        for ($i=0; $i < count($request->id); $i++) { 
            $asset_detail = AssetDetail::find($request->id[$i]);
            $asset_detail->pencairan = str_replace(',','',$request->pencairan[$i]);
            $asset_detail->angsuran = str_replace(',','',$request->angsuran[$i]);
            $asset_detail->save();
        }
    }


}
