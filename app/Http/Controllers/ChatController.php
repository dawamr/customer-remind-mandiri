<?php

namespace App\Http\Controllers;

use App\Chat;
use App\ChatConfig;
use App\Asset;
use Auth;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $asset_set =null)
    {
        $configs =  ChatConfig::get();
        $lastChat = Chat::orderBy('updated_at', 'desc')->where('user_id', Auth::id())->limit(1)->first();
        $arrData =  [];
        for ($i=0; $i < count($configs); $i++) { 
            $arrData[$i] = $configs[$i]->variable;
        }
        $data['arrData'] = $arrData;
        $data['configs'] = $configs;
        $data['lastChat'] = $lastChat;
        return view('settings.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $text  = $request->message;
        $find = array("<p>", "</p>", "<b>", "</b>", "<i>", "</i>", "<strike>", "</strike>", "<br>");
        $replace   = array("", "%0A", "*", "*", "_", "_","~", "~", "%0A");

        $whatsappFormat = str_replace($find, $replace, $text);
        $cek = Chat::orderBy('updated_at', 'desc')->get();
        if(count($cek) == 0){
            $data = new Chat;
        }else{
            $data = Chat::find($cek[0]->id);
        }
        $data->title = Date('Y-m-d');
        $data->message_whatsapp = $whatsappFormat;
        $data->message_html = $request->message;
        $data->user_id = Auth::id();
        $data->type = 'chat';
        $data->status = 'active';
        $data->save();
        return response()->json($whatsappFormat, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(Chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat  $chat
     * @return \Illuminate
     * \Http\Response
     */
    public function destroy(Chat $chat)
    {
        //
    }

    public function desroy_variable($id){
        $data = ChatConfig::find($id);
        $data->delete();
        return  back()->with('success', 'Success delete variable');
        
    }

    public function bulkSend(){
        $customers = Customer::orderBy('name','asc')->get();
        $data['customers'] = $customers;
    }
    

    // public function settingAssets(Request $request){
    //     try {
    //         $asset = Asset::find($request->asset_id);
    //         $asset->config = $asset->asset_details;
    //         $asset_config_id = null;
            
    //         for ($i=0; $i < count($asset->config); $i++) { 
    //             if($asset->config[$i]->type == $request->type && $asset->config[$i]->tenor == $request->tenor){
    //                 $asset_config_id = $asset->config[$i]->id;
    //             }
    //         }

    //         $data = Chat::where('user_id', Auth::id())->orderBy('updated_at')->limit(1)->first();
    //         if($data == null){
    //             $data = new Chat;
    //         }
    //         $data->title = Date('Y-m-d');
    //         $data->asset_id = $request->asset_id;
    //         $data->asset_config_id = $asset_config_id;
    //         $data->user_id = Auth::id();
            
    //         if($data->save()){
    //             return  redirect('admin/settings')->with('success', 'Success create update set asset');
    //         }
    //     } catch (Expection $err) {
    //         return  back()->with('wrong', 'Failed! '. $err->getMessage());
    //     }
    // }
}
