<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FunctionController extends Controller
{
    public function formatPhoneNumber($phoneNumber){
        $text = str_split($phoneNumber);
        $y=null;
        $x=0;
        if (count($text) >1) {
            if($text[0] == '6'){
                for($i=0; $i <count($text); $i++){
                    $y .= $text[$i];
                }
            }
            if($text[0] == '0'){
                $y = str_replace('0','62',$text[0]);
                for($i=1; $i <count($text); $i++){
                    $y .= $text[$i];
                }
            }
            if($text[0] == '+'){
                $y= str_replace('+','',$phoneNumber);
            }
        }

        return $y;
    }

    public function sendWA($phone, $message){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://app.whatspie.com/api/messages",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "receiver=".$phone."&device=".env('APP_PHONE_DEVICE')."&message=".nl2br($message)."&type=chat",
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Bearer ". env('APP_WA_KEY')
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function checkYear($tenor){
        if($tenor <=12){
            return 1;
        }
        if($tenor > 12 && $tenor <= 24){
            return 2;
        }
        if($tenor > 24 && $tenor <= 36){
            return 3;
        }
        if($tenor > 36 && $tenor <= 48){
            return 4;
        }
        if($tenor > 49 && $tenor <= 60){
            return 5;
        }
        if($tenor > 60 && $tenor <= 72){
            return 6;
        }
    }
}
